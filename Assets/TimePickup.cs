using UnityEngine;

public class TimePickup : MonoBehaviour {
    public PickupList pickupList;
    private void OnEnable() {
        pickupList.Add(this);
    }
    private void OnDisable() {
        pickupList.Remove(this);
    }
}
