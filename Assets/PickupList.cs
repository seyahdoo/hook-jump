using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class PickupList : ScriptableObject {
    private HashSet<TimePickup> _set = new HashSet<TimePickup>();
    public void Add(TimePickup timePickup) {
        _set.Add(timePickup);
    }
    public void Remove(TimePickup timePickup) {
        _set.Remove(timePickup);
    }
    public HashSet<TimePickup> GetSet() {
        return _set;
    }
}
