using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

    public float initialRemainingTime = 5f;
    public TimePickupSpawner timePickupSpawner;
    public TextMeshProUGUI remainingTimeText;
    public TextMeshProUGUI totalCollectedText;
    public TextMeshProUGUI totalCollectedTextOnHighScore;
    public TextMeshProUGUI highScoreTableText;
    public Player player;
    public GameObject gameUI;
    public GameObject highScoreScreen;
    public Button highScoreSubmitButton;
    public Button restartButton;
    public TMP_InputField nameInputField;
    
    public float remainingTime;
    private int _totalCollected = 0;
    private bool _isPlaying = true;
    private List<HighScore> _highScores = new List<HighScore>();
    private struct HighScore {
        public string name;
        public int score;
    }
    private void Awake() {
        remainingTime = initialRemainingTime;
        highScoreScreen.SetActive(false);
        gameUI.SetActive(true);
        highScoreSubmitButton.onClick.AddListener(HighScoreSubmit);
        restartButton.onClick.AddListener(RestartLevel);
        LoadHighScores();
        RefreshHighScoresView();
    }
    private void RestartLevel() {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
    private void HighScoreSubmit() {
        var playerName = nameInputField.text;
        if (string.IsNullOrWhiteSpace(playerName)) {
            return;
        }
        if (playerName.Contains(";")) {
            return;
        }
        _highScores.Add(new HighScore() {name = playerName, score = _totalCollected});
        _highScores.Sort(((score, highScore) => score.score - highScore.score));
        SaveHighScores();
        RefreshHighScoresView();
        highScoreSubmitButton.interactable = false;
    }
    private void SaveHighScores() {
        var str = Serialize(_highScores);
        PlayerPrefs.SetString("highscores", str);
        PlayerPrefs.Save();
    }
    private void LoadHighScores() {
        var str = PlayerPrefs.GetString("highscores", "");
        _highScores = Deserialize(str);
    }
    private void RefreshHighScoresView() {
        var text = "";
        text += "High Scores\n";
        text += "\n";
        foreach (var highScore in _highScores) {
            text += $"{highScore.name} : {highScore.score}\n";
        }
        highScoreTableText.text = text;
    }
    private List<HighScore> Deserialize(string text) {
        var list = new List<HighScore>();
        var split = text.Split(new [] { ";" }, StringSplitOptions.RemoveEmptyEntries);
        for (int i = 0; i < split.Length; i += 2) {
            var name = split[i];
            var score = int.Parse(split[i + 1]);
            list.Add(new HighScore(){name = name, score = score});
        }
        return list;
    }
    private string Serialize(List<HighScore> highScores) {
        var str = "";
        foreach (var highScore in highScores) {
            str += $"{highScore.name};{highScore.score};";
        }
        str = str.TrimEnd(';');
        return str;
    }
    
    public void OnTimePickupPickedUp() {
        if (!_isPlaying) {
            return;
        }
        _totalCollected += 1;
        totalCollectedText.text = _totalCollected.ToString();
        totalCollectedTextOnHighScore.text = _totalCollected.ToString();
        remainingTime += 5f;
        timePickupSpawner.OnTimePickupPickedUp();
    }
    private void Update() {
        remainingTime -= Time.deltaTime;
        if (remainingTime < 0) {
            remainingTime = 0f;
            _isPlaying = false;
            gameUI.SetActive(false);
            highScoreScreen.SetActive(true);
        }
        remainingTimeText.text = remainingTime.ToString(CultureInfo.InvariantCulture);
    }
}
