using UnityEngine;

public class Player : MonoBehaviour {
    public Camera cam;
    public float stickPower;
    public float jumpMultiplier = 1f;
    public float stickCooldownAfterJump = .1f;
    public float chargeSpeed = 1f;
    [Range(0f,1f)] public float currentCharge;
    public Transform arrow;
    public AnimationCurve stretchCurve;

    public Sprite happySprite;
    public Sprite surprisedSprite;
    public Sprite sadSprite;
    public Sprite neutralSprite;
    
    private bool _isSticked = false;
    private Rigidbody2D _body;
    private Collider2D _stickedCollider;
    private float _unstickStartTime = 0f;
    private GameManager _gameManager;
    private float _faceLastChangeTime;
    private SpriteRenderer _spriteRenderer;
    private float _stretchStartTime;
    private bool _wasLastStickAxisX;
    
    private void Awake() {
        _body = GetComponent<Rigidbody2D>();
        _gameManager = FindObjectOfType<GameManager>();
        _spriteRenderer = GetComponent<SpriteRenderer>();
    }
    private void Update() {
        if (_isSticked) {
            if (Input.GetMouseButton(0)) {
                currentCharge = Mathf.Clamp01(currentCharge + (Time.deltaTime * chargeSpeed));
                arrow.gameObject.SetActive(true);
                arrow.position = transform.position;
                var direction = GetFlyDirection();
                arrow.rotation = Quaternion.Euler(0,0,Vector2.SignedAngle(Vector2.up, direction));
                arrow.localScale = new Vector3(1f, .5f + currentCharge, 1f);
                _wasLastStickAxisX = IsStickAxisX();
                transform.localScale = Vector3.one - (_wasLastStickAxisX?Vector3.right:Vector3.up) * (stretchCurve.Evaluate(-currentCharge));
            }
            else if (Input.GetMouseButtonUp(0)) {
                _isSticked = false;
                _unstickStartTime = Time.time;
                FlyToMouse();
                transform.localScale = Vector3.one;
                _stretchStartTime = Time.time;
            }
            else {
                transform.localScale = Vector3.one;
            }
        }
        else {
            arrow.gameObject.SetActive(false);
            var timePassedSinceLastStretch = Time.time - _stretchStartTime;
            transform.localScale = Vector3.one - (_wasLastStickAxisX?Vector3.right:Vector3.up) * (stretchCurve.Evaluate(timePassedSinceLastStretch));
        }
        if (Time.time > _faceLastChangeTime + .5f) {
            _spriteRenderer.sprite = neutralSprite;
            _faceLastChangeTime = Time.time;
        }
        
    }
    private void FixedUpdate() {
        if (_isSticked) {
            var shouldContactPoint = _stickedCollider.ClosestPoint(transform.position);
            _body.AddForce((shouldContactPoint - _body.position).normalized * stickPower);
        }
    }
    private bool IsStickAxisX() {
        var shouldContactPoint = _stickedCollider.ClosestPoint(transform.position);
        var localShouldContactPoint = transform.InverseTransformPoint(shouldContactPoint);
        return Mathf.Abs(localShouldContactPoint.x) > Mathf.Abs(localShouldContactPoint.y);
    }
    private Vector2 GetFlyDirection() {
        var ray = cam.ScreenPointToRay(Input.mousePosition);
        var plane = new Plane(Vector3.forward, Vector3.zero);
        plane.Raycast(ray, out float enter);
        Vector2 targetWorld = ray.GetPoint(enter);
        return (targetWorld - (Vector2) transform.position).normalized;
    }
    private void FlyToMouse() {
        var direction = GetFlyDirection();
        var force = direction * currentCharge * jumpMultiplier;
        currentCharge = 0f;
        _body.AddForce(force, ForceMode2D.Impulse);

        _spriteRenderer.sprite = happySprite;
        _faceLastChangeTime = Time.time;
    }
    private void OnCollisionStay2D(Collision2D col) {
        DealWithCollision(col);
    }
    private void OnCollisionEnter2D(Collision2D col) {
        DealWithCollision(col);
        if (col.relativeVelocity.magnitude > .3f) {
            _faceLastChangeTime = Time.time;
            _spriteRenderer.sprite = surprisedSprite;
        }
    }
    private void DealWithCollision(Collision2D col) {
        if (!_isSticked && Time.time > _unstickStartTime + stickCooldownAfterJump) {
            _isSticked = true;
            _stickedCollider = col.collider;
        }
    }
    private void OnTriggerEnter2D(Collider2D col) {
        var timePickup = col.GetComponent<TimePickup>();
        if (timePickup != null) {
            Destroy(col.gameObject);
            _gameManager.OnTimePickupPickedUp();
        }
    }
}
