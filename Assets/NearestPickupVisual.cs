using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NearestPickupVisual : MonoBehaviour {

    public Transform arrow;
    public PickupList pickupList;
    public Player player;
    public Camera camera;

    private void Update() {
        TimePickup nearestPickup = null;
        float nearestPickupDistance = float.MaxValue;
        foreach (var pickup in pickupList.GetSet()) {
            var distance = Vector2.Distance(player.transform.position, pickup.transform.position);
            if (distance < nearestPickupDistance) {
                nearestPickupDistance = distance;
                nearestPickup = pickup;
            }
        }
        if (nearestPickup == null) {
            arrow.gameObject.SetActive(false);
            return;
        }
        var screenPoint = camera.WorldToScreenPoint(nearestPickup.transform.position);
        screenPoint.x = Mathf.Clamp(screenPoint.x, 20, Screen.width - 20);
        screenPoint.y = Mathf.Clamp(screenPoint.y, 20, Screen.height - 20);
        var arrowPos = camera.ScreenToWorldPoint(screenPoint);
        arrow.position = arrowPos;

        var diff = (Vector2) nearestPickup.transform.position - (Vector2) arrow.position;
        var arrowDirection = diff.normalized;
        arrow.rotation = Quaternion.Euler(0,0,Vector2.SignedAngle(Vector2.up, arrowDirection));
        arrow.gameObject.SetActive(diff.magnitude > 1f);
    }
}
