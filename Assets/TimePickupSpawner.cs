using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using Random = UnityEngine.Random;

public class TimePickupSpawner : MonoBehaviour {
    public Transform max;
    public Transform min;
    public TimePickup timePickupPrefab;
    public Player player;
    public AnimationCurve distanceCurve;
    private int _collectedCount = 0;
    public void OnTimePickupPickedUp() {
        _collectedCount++;
        var tryCount = 100;
        while (tryCount-- > 0) {
            var newRandomPosition = (Vector2)player.transform.position + Random.insideUnitCircle * distanceCurve.Evaluate(_collectedCount);
            if (!Physics2D.OverlapCircle(newRandomPosition, .1f)) {
                Instantiate(timePickupPrefab, newRandomPosition, quaternion.identity);
                return;
            }
        }
    }
}
